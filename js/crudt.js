import { productosMostrar as endpoint } from "../js/urls.js";

//CRUDT

//capturar datos para buscar / guaradar


const capturarDatos = () => {
    const nombre = document.getElementById('InputName').value;
    const imagenPpal = document.getElementById('InputImagenPpal').value;
    const imagen1 = document.getElementById('InputImagen1').value;
    const imagen2 = document.getElementById('InputImagen2').value;
    const descripcion = document.getElementById('InputDescripcion').value;
    const precio = document.getElementById('InputPrecio').value;


    const user = {
        nombre,
        imagenPpal,
        imagen1,
        imagen2,
        descripcion,
        precio,
    }
    return user;
}

//Escuchador eventos

const form = document.querySelector('.form-productos');


//nuevo producto

form.addEventListener('submit', async (e) => {
    e.preventDefault();
    const objeto = capturarDatos();

    await fetch(endpoint, {
        method: 'POST',
        body: JSON.stringify(objeto),
        headers: {
            "Content-Type": "application/json; charset=utf-8"
        }
    })

})

//Eliminar producto

form.addEventListener('click', async (e) => {
    const btnEliminar = e.target.classList.contains('btneliminar');
    console.log(btnEliminar)
    if (btnEliminar === true) {
        const id = e.target.id;
        await fetch(endpoint + id, {
            method: 'DELETE'
        })
    }

})

//Buscar producto por nombre

const btnCorreo = document.getElementById('btn-buscar');

btnCorreo.addEventListener('click', async () => {

    const input = document.getElementById('InputName').value;
    const resp = await fetch(endpoint);
    const lista = await resp.json()
    const busqueda = lista.find(x => x.nombre.toLocaleLowerCase() === input.toLocaleLowerCase())
    if (busqueda !== undefined) {
        const { id, nombre, descripcion, imagenPpal, imagen1, imagen2, precio } = busqueda;
        document.getElementById('InputName').value = nombre;
        document.getElementById('InputImagenPpal').value = imagenPpal;
        document.getElementById('InputImagen1').value = imagen1;
        document.getElementById('InputImagen2').value = imagen2;
        document.getElementById('InputDescripcion').value = descripcion;
        document.getElementById('InputPrecio').value = precio;
        document.getElementById('inputId').value = id;
    } else {
        alert('Producto no encontrado')
    }
})

//Modificar producto econtrado

const btnModificar = document.getElementById('btn-modificar');

btnModificar.addEventListener('click', async () => {

    const dataMod = capturarDatos();
    const { nombre, imagenPpal, imagen1, imagen2, descripcion, precio, id } = dataMod;

    if (nombre === "", imagenPpal === "", imagen1 === "", imagen2 === "", descripcion === "", precio === "") {
        alert('Llenar todos los campos')
    }
    else {
        const id = document.getElementById('inputId').value;
        await fetch(endpoint + id, {
            method: 'PUT',
            body: JSON.stringify(dataMod),
            headers: {
                "Content-Type": "application/json; charset=utf-8"
            }
        })
    }

})