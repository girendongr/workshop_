import { productosMostrar as endpoint } from "../js/urls.js";
import { getData } from "../js/getData.js";
import { showData } from "../js/showData.js";

const elemento = document.querySelector("#collecttion"); 

document.addEventListener("DOMContentLoaded", () => {
    const data = getData(endpoint);
    showData(data, elemento);
});

//Cuando haga click en el .list-group captureme el evento
elemento.addEventListener("click", async (e) => {
    const id = e.target.id; //capturamos el id y ya consegui el id
    const btnDetail = e.target.classList.contains("btn-primary"); //capturamos la clase del boton. contains nos devuelve un booleano
    //console.log(btnDetail)

    //Si hago click en brnDetail traigame toda la data
    if (btnDetail) {
        const lista = await getData(endpoint); //Me queda almacenado una promesa por resolver
        //console.log("hizo click en el boton detail");
        //utilizo find para buscar en la lista el elemento del objeto que queremos buscar
        const objeto = lista.find((list) => list.id === Number(id));
        //console.log(objeto);
        //Almacenar en el localStorage
        localStorage.setItem("Detalles", JSON.stringify(objeto));
        window.location.href = "info.html"; //Aca relaciono la pagina donde se van a pintar los datos del detail
    }
});