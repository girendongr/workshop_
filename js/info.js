const listGroup = document.querySelector("#collecttion");

const getLocalStorage = () => {
  const det = JSON.parse(localStorage.getItem("Detalles"));
  const { nombre, descripcion, precio, imagenPpal, imagen1, imagen2, id } = det;

  listGroup.innerHTML += `
    <div class="container">
                          <div class="row">
                          <div class="col-2">
                          <a href="#" onClick="change('${imagenPpal}')" class="d-flex justify-content-center">
                          <img style="width:100px" src="${imagenPpal}" alt=""></a>
                          <a href="#" onClick="change('${imagen1}')" class="d-flex justify-content-center mt-3">
                          <img style="width:100px" src="${imagen1}" alt=""></a>
                          <a href="#" onClick="change('${imagen2}')" class="d-flex justify-content-center mt-3">
                          <img style="width:100px" src="${imagen2}" alt=""></a>
                          </div>
                          <div class="col-5">
                          <img id="imageChange" style="width:80%" src="${imagenPpal}" alt=""></div>
                          <div class="col-5">
                          <strong><h3 class="d-flex justify-content-left">${nombre}</h3></strong>
                          <h5 class="d-flex justify-content-left">$ ${precio}.00</h5>
                          <span class="d-flex mt-4">Size</span>
                          <div class="d-flex mt-3">
                          <a Style="text-decoration: none; color:black;" href="#" class="d-row p-3">S</a>
                          <a Style="text-decoration: none; color:black;" href="#" class="d-row p-3">M</a>
                          <a Style="text-decoration: none; color:black;" href="#" class="d-row p-3">L</a>
                          <a Style="text-decoration: none; color:black;" href="#" class="d-row p-3">XL</a>
                          <a Style="text-decoration: none; color:black;" href="#" class="d-row p-3">XXL</a>
                          </div>
                          <a href="#" id="${id}" class="btn btn-dark d-flex justify-content-center mt-4">ADD TO CART</a>
                          <span class="d-flex justify-content-left mt-2"> ${descripcion}</span></div></div>
    `;
};

document.addEventListener("DOMContentLoaded", getLocalStorage);

listGroup.addEventListener("click", (e) => {
  const introducirCarrito = document.getElementById("carritoUl");
  const detalles = JSON.parse(localStorage.getItem("Detalles"));
  let idCarrito = detalles.id;
  let imgCarrito = detalles.imagenPpal;
  let nombreCarrito = detalles.nombre;
  let precioCarrito = detalles.precio;
  let carrito = {
    idCarrito,
    imgCarrito,
    nombreCarrito,
    precioCarrito,
  };
  console.log(carrito);

  let getId = e.target.classList.contains("btn-dark");
  if (getId === true) {
    localStorage.removeItem("Detalles");
    localStorage.setItem("carrito", JSON.stringify(carrito));
    introducirCarrito.innerHTML = `
    <li class="d-flex row">
                <img
                  src="${imgCarrito}"
                  alt=""
                  style="height: 200px; width: 200px"
                  class="img me-2"
                />
                <p>${nombreCarrito}</p>
                <br />
                <p class="col">${precioCarrito}</p>
              </li>
    `;
  }
});
