export const getData = async (url) => {
    const respuestaPromesa = await fetch(url);
    const datos = await respuestaPromesa.json();
    return datos;
};
