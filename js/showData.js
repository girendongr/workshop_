export const showData = async (data, elemento) => {
  const productos = await data; 
  productos.forEach((prod) => {
    const { id, nombre, imagenPpal } = prod;
    elemento.innerHTML += `
        <div class="container card" style="width: 14rem;" id="cardProducto">
            <img src="${imagenPpal}" class="card-img-top mt-3" alt="...">
            <div class="card-body">
                <h5 class="card-title d-flex justify-content-center"><strong>${nombre}</strong></h5>
                <a href="#" id=${id} class="btn btn-primary d-flex justify-content-center">BUY IT</a>
            </div>
        `;
  });
};
